package com.miyagi;

public class Projects extends ProjectOut {
    protected String nameOfProject;
    protected int price;

    public Projects(String nameOfProject,int price){
this.nameOfProject=nameOfProject;
this.price=price;
    }
    public String getNameOfProject() {
        return nameOfProject;
    }

    public void setNameOfProject(String nameOfProject) {
        this.nameOfProject = nameOfProject;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
    return "Name of Project: "+nameOfProject+" price: "+price;
    }
}
